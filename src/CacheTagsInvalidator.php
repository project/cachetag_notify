<?php

namespace Drupal\cachetag_notify;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;

/**
 * Cache tags invalidator implementation that notifies a thirdparty.
 */
class CacheTagsInvalidator implements CacheTagsInvalidatorInterface {

  /**
   * Drupal Config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger instance for the api module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerService;

  /**
   * Constructs a CacheTagsInvalidator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal Config.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_service
   *   Logger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
   ClientInterface $http_client,
   LoggerChannelFactoryInterface $logger_service) {
    $this->config = $config_factory->get('cachetag_notify.settings');
    $this->httpClient = $http_client;
    $this->loggerService = $logger_service;
    $this->logger = $this->loggerService->get('cache_tag_notify');
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags) {
    $endpoint_url = $this->config->get('endpoint');
    if (empty($endpoint_url)) {
      $this->logger->error('No endpoint set');
      return;
    }

    try {
      $this->httpClient->post($endpoint_url, ['body' => json_encode($tags)]);
    }
    catch (ClientException $e) {
      watchdog_exception('CacheTag Notify', $e);
    }
    catch (ServerException $e) {
      watchdog_exception('CacheTag Notify', $e);
    }
    catch (ConnectException $e) {
      watchdog_exception('CacheTag Notify', $e);
    }
    catch (Exception $e) {
      watchdog_exception('CacheTag Notify', $e);
    }
  }

}
