# CacheTag Notify

- Notifies a given URL with a JSON string of invalidated cache tags.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- Notifies a given URL with a JSON string of invalidated cache tags.

## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Enable the CacheTag Notify module.
- Go to `/admin/config/system/cachetag_notify` and add endpoint URL.
- That's it! You'll now be receiving JSON to your endpoint on each cache tag invalidation.

## Maintainers

- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- timmillwood - [timmillwood](https://www.drupal.org/u/timmillwood)
- Dick Olsson - [dixon_](https://www.drupal.org/u/dixon_)
